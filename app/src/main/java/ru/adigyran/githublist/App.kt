package ru.adigyran.githublist

import org.greenrobot.eventbus.EventBus
import org.kodein.di.Kodein
import org.kodein.di.android.androidCoreModule
import org.kodein.di.generic.bind
import org.kodein.di.generic.eagerSingleton
import ru.adigyran.githublist.di.apiDiModule
import ru.adigyran.githublist.di.interactorsDiModule
import ru.adigyran.githublist.di.mappersDiModule
import ru.adigyran.githublist.di.netDiModule
import ru.ivan.core.BaseApp
import ru.ivan.core.di.managersCoreDiModule
import ru.ivan.core.di.navigationDiModule

class App : BaseApp() {



    override val kodein: Kodein = Kodein.lazy {
        import(androidCoreModule(this@App))
        import(navigationDiModule())
        import(netDiModule(this@App))
        import(managersCoreDiModule())
        import(interactorsDiModule())
        import(mappersDiModule())
        import(apiDiModule())
        bind<EventBus>() with eagerSingleton { EventBus.getDefault() }

    }

    override fun onCreate() {
        super.onCreate()
        appInstance = this

    }
    companion object {

        lateinit var appInstance: App
            private set
    }

}