package ru.adigyran.githublist.server.response


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class RepoResp(
    @Json(name = "id")
    val id: Int?, // 95289218
    @Json(name = "node_id")
    val nodeId: String?, // MDEwOlJlcG9zaXRvcnk5NTI4OTIxOA==
    @Json(name = "name")
    val name: String?, // angular-laravel
    @Json(name = "full_name")
    val fullName: String?, // adigu/angular-laravel
    @Json(name = "private")
    val `private`: Boolean?, // false
    @Json(name = "owner")
    val owner: Owner?,
    @Json(name = "html_url")
    val htmlUrl: String?, // https://github.com/adigu/angular-laravel
    @Json(name = "description")
    val description: String?, // tugas_2
    @Json(name = "fork")
    val fork: Boolean?, // false
    @Json(name = "url")
    val url: String?, // https://api.github.com/repos/adigu/angular-laravel
    @Json(name = "forks_url")
    val forksUrl: String?, // https://api.github.com/repos/adigu/angular-laravel/forks
    @Json(name = "keys_url")
    val keysUrl: String?, // https://api.github.com/repos/adigu/angular-laravel/keys{/key_id}
    @Json(name = "collaborators_url")
    val collaboratorsUrl: String?, // https://api.github.com/repos/adigu/angular-laravel/collaborators{/collaborator}
    @Json(name = "teams_url")
    val teamsUrl: String?, // https://api.github.com/repos/adigu/angular-laravel/teams
    @Json(name = "hooks_url")
    val hooksUrl: String?, // https://api.github.com/repos/adigu/angular-laravel/hooks
    @Json(name = "issue_events_url")
    val issueEventsUrl: String?, // https://api.github.com/repos/adigu/angular-laravel/issues/events{/number}
    @Json(name = "events_url")
    val eventsUrl: String?, // https://api.github.com/repos/adigu/angular-laravel/events
    @Json(name = "assignees_url")
    val assigneesUrl: String?, // https://api.github.com/repos/adigu/angular-laravel/assignees{/user}
    @Json(name = "branches_url")
    val branchesUrl: String?, // https://api.github.com/repos/adigu/angular-laravel/branches{/branch}
    @Json(name = "tags_url")
    val tagsUrl: String?, // https://api.github.com/repos/adigu/angular-laravel/tags
    @Json(name = "blobs_url")
    val blobsUrl: String?, // https://api.github.com/repos/adigu/angular-laravel/git/blobs{/sha}
    @Json(name = "git_tags_url")
    val gitTagsUrl: String?, // https://api.github.com/repos/adigu/angular-laravel/git/tags{/sha}
    @Json(name = "git_refs_url")
    val gitRefsUrl: String?, // https://api.github.com/repos/adigu/angular-laravel/git/refs{/sha}
    @Json(name = "trees_url")
    val treesUrl: String?, // https://api.github.com/repos/adigu/angular-laravel/git/trees{/sha}
    @Json(name = "statuses_url")
    val statusesUrl: String?, // https://api.github.com/repos/adigu/angular-laravel/statuses/{sha}
    @Json(name = "languages_url")
    val languagesUrl: String?, // https://api.github.com/repos/adigu/angular-laravel/languages
    @Json(name = "stargazers_url")
    val stargazersUrl: String?, // https://api.github.com/repos/adigu/angular-laravel/stargazers
    @Json(name = "contributors_url")
    val contributorsUrl: String?, // https://api.github.com/repos/adigu/angular-laravel/contributors
    @Json(name = "subscribers_url")
    val subscribersUrl: String?, // https://api.github.com/repos/adigu/angular-laravel/subscribers
    @Json(name = "subscription_url")
    val subscriptionUrl: String?, // https://api.github.com/repos/adigu/angular-laravel/subscription
    @Json(name = "commits_url")
    val commitsUrl: String?, // https://api.github.com/repos/adigu/angular-laravel/commits{/sha}
    @Json(name = "git_commits_url")
    val gitCommitsUrl: String?, // https://api.github.com/repos/adigu/angular-laravel/git/commits{/sha}
    @Json(name = "comments_url")
    val commentsUrl: String?, // https://api.github.com/repos/adigu/angular-laravel/comments{/number}
    @Json(name = "issue_comment_url")
    val issueCommentUrl: String?, // https://api.github.com/repos/adigu/angular-laravel/issues/comments{/number}
    @Json(name = "contents_url")
    val contentsUrl: String?, // https://api.github.com/repos/adigu/angular-laravel/contents/{+path}
    @Json(name = "compare_url")
    val compareUrl: String?, // https://api.github.com/repos/adigu/angular-laravel/compare/{base}...{head}
    @Json(name = "merges_url")
    val mergesUrl: String?, // https://api.github.com/repos/adigu/angular-laravel/merges
    @Json(name = "archive_url")
    val archiveUrl: String?, // https://api.github.com/repos/adigu/angular-laravel/{archive_format}{/ref}
    @Json(name = "downloads_url")
    val downloadsUrl: String?, // https://api.github.com/repos/adigu/angular-laravel/downloads
    @Json(name = "issues_url")
    val issuesUrl: String?, // https://api.github.com/repos/adigu/angular-laravel/issues{/number}
    @Json(name = "pulls_url")
    val pullsUrl: String?, // https://api.github.com/repos/adigu/angular-laravel/pulls{/number}
    @Json(name = "milestones_url")
    val milestonesUrl: String?, // https://api.github.com/repos/adigu/angular-laravel/milestones{/number}
    @Json(name = "notifications_url")
    val notificationsUrl: String?, // https://api.github.com/repos/adigu/angular-laravel/notifications{?since,all,participating}
    @Json(name = "labels_url")
    val labelsUrl: String?, // https://api.github.com/repos/adigu/angular-laravel/labels{/name}
    @Json(name = "releases_url")
    val releasesUrl: String?, // https://api.github.com/repos/adigu/angular-laravel/releases{/id}
    @Json(name = "deployments_url")
    val deploymentsUrl: String?, // https://api.github.com/repos/adigu/angular-laravel/deployments
    @Json(name = "created_at")
    val createdAt: String?, // 2017-06-24T09:50:05Z
    @Json(name = "updated_at")
    val updatedAt: String?, // 2017-06-24T09:50:05Z
    @Json(name = "pushed_at")
    val pushedAt: String?, // 2017-06-24T09:50:05Z
    @Json(name = "git_url")
    val gitUrl: String?, // git://github.com/adigu/angular-laravel.git
    @Json(name = "ssh_url")
    val sshUrl: String?, // git@github.com:adigu/angular-laravel.git
    @Json(name = "clone_url")
    val cloneUrl: String?, // https://github.com/adigu/angular-laravel.git
    @Json(name = "svn_url")
    val svnUrl: String?, // https://github.com/adigu/angular-laravel
    @Json(name = "homepage")
    val homepage: Any?, // null
    @Json(name = "size")
    val size: Int?, // 0
    @Json(name = "stargazers_count")
    val stargazersCount: Int?, // 0
    @Json(name = "watchers_count")
    val watchersCount: Int?, // 0
    @Json(name = "language")
    val language: Any?, // null
    @Json(name = "has_issues")
    val hasIssues: Boolean?, // true
    @Json(name = "has_projects")
    val hasProjects: Boolean?, // true
    @Json(name = "has_downloads")
    val hasDownloads: Boolean?, // true
    @Json(name = "has_wiki")
    val hasWiki: Boolean?, // true
    @Json(name = "has_pages")
    val hasPages: Boolean?, // false
    @Json(name = "forks_count")
    val forksCount: Int?, // 0
    @Json(name = "mirror_url")
    val mirrorUrl: Any?, // null
    @Json(name = "archived")
    val archived: Boolean?, // false
    @Json(name = "disabled")
    val disabled: Boolean?, // false
    @Json(name = "open_issues_count")
    val openIssuesCount: Int?, // 0
    @Json(name = "license")
    val license: Any?, // null
    @Json(name = "forks")
    val forks: Int?, // 0
    @Json(name = "open_issues")
    val openIssues: Int?, // 0
    @Json(name = "watchers")
    val watchers: Int?, // 0
    @Json(name = "default_branch")
    val defaultBranch: String? // master
) {
    @JsonClass(generateAdapter = true)
    data class Owner(
        @Json(name = "login")
        val login: String?, // adigu
        @Json(name = "id")
        val id: Int?, // 28130907
        @Json(name = "node_id")
        val nodeId: String?, // MDQ6VXNlcjI4MTMwOTA3
        @Json(name = "avatar_url")
        val avatarUrl: String?, // https://avatars2.githubusercontent.com/u/28130907?v=4
        @Json(name = "gravatar_id")
        val gravatarId: String?,
        @Json(name = "url")
        val url: String?, // https://api.github.com/users/adigu
        @Json(name = "html_url")
        val htmlUrl: String?, // https://github.com/adigu
        @Json(name = "followers_url")
        val followersUrl: String?, // https://api.github.com/users/adigu/followers
        @Json(name = "following_url")
        val followingUrl: String?, // https://api.github.com/users/adigu/following{/other_user}
        @Json(name = "gists_url")
        val gistsUrl: String?, // https://api.github.com/users/adigu/gists{/gist_id}
        @Json(name = "starred_url")
        val starredUrl: String?, // https://api.github.com/users/adigu/starred{/owner}{/repo}
        @Json(name = "subscriptions_url")
        val subscriptionsUrl: String?, // https://api.github.com/users/adigu/subscriptions
        @Json(name = "organizations_url")
        val organizationsUrl: String?, // https://api.github.com/users/adigu/orgs
        @Json(name = "repos_url")
        val reposUrl: String?, // https://api.github.com/users/adigu/repos
        @Json(name = "events_url")
        val eventsUrl: String?, // https://api.github.com/users/adigu/events{/privacy}
        @Json(name = "received_events_url")
        val receivedEventsUrl: String?, // https://api.github.com/users/adigu/received_events
        @Json(name = "type")
        val type: String?, // User
        @Json(name = "site_admin")
        val siteAdmin: Boolean? // false
    )
}