package ru.adigyran.githublist.ui.delegates

import android.view.LayoutInflater
import android.view.ViewGroup
import ru.adigyran.githublist.R
import ru.adigyran.githublist.databinding.ItemUserRepoBinding
import ru.ivan.core.ext.bindClick
import ru.ivan.core.ui.adapters.base.BaseDelegate
import ru.ivan.core.ui.adapters.base.BaseViewHolder
import ru.ivan.core.ui.adapters.base.DiffItem

class UserRepoItemDelegate(private val itemClick: ((Int) -> Unit)?):BaseDelegate<UserRepoItemUIModel>() {

    override fun isForViewType(
            item: DiffItem, items: MutableList<DiffItem>, position: Int
    ): Boolean =
            item is UserRepoItemUIModel

    override fun onCreateViewHolder(parent: ViewGroup): ViewHolder {
        val binding =
                ItemUserRepoBinding.inflate(LayoutInflater.from(parent.context), parent, false)

        return ViewHolder(binding)
    }

    inner class ViewHolder(private val binding: ItemUserRepoBinding) :
            BaseViewHolder<UserRepoItemUIModel>(binding.root) {

        init {
            binding.root bindClick { itemClick?.invoke(item.id) }
        }

        override fun bind(item: UserRepoItemUIModel) {
            super.bind(item)
            binding.tvRepoName.text = item.repoName
            binding.tvRepoFullName.text = item.fullName
            binding.tvForks.text = binding.root.resources.getString(R.string.forks_format).format(item.forksCount)
            binding.tvStars.text = binding.root.resources.getString(R.string.stars_format).format(item.stargazersCount)
            binding.tvWatchers.text = binding.root.resources.getString(R.string.watchers_format).format(item.watchersCount)
        }
    }
}

class UserRepoItemUIModel(
        val id:Int,
        val repoName:String,
                          val fullName: String,
                          val forksCount: Int,
                          val stargazersCount: Int,
                          val watchersCount: Int
                          ) : DiffItem{
    override fun areItemsTheSame(newItem: DiffItem): Boolean =
            newItem is UserRepoItemUIModel
                    && newItem.repoName == repoName
                    && newItem.fullName == fullName
                    && newItem.forksCount == forksCount
                    && newItem.stargazersCount == stargazersCount
                    && newItem.watchersCount == watchersCount


    override fun areContentsTheSame(newItem: DiffItem): Boolean = newItem is UserRepoItemUIModel
}