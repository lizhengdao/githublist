package ru.adigyran.githublist.data.interactors

import io.reactivex.Observable
import ru.adigyran.githublist.data.mappers.interfaces.IRepositoryItemModelMapper
import ru.adigyran.githublist.data.models.classes.RepositoryItemModel
import ru.adigyran.githublist.server.Api
import ru.adigyran.githublist.util.Utils
import ru.ivan.core.ext.asResult
import ru.ivan.core.ext.mapResult
import ru.ivan.core.ext.retrofitResponseToResult
import ru.ivan.core.ext.retryOnError
import ru.ivan.core.server.base.RequestResult

class RepositoriesInteractor(private val api: Api,
                             private val repositoryItemModelMapper: IRepositoryItemModelMapper) {



    fun getUserRepositories(userName:String):Observable<RequestResult<List<RepositoryItemModel>>>  =
            api.getUserRepos(userName)
                    .retryOnError(null)
                    .retrofitResponseToResult(Utils::mapServerErrors)
                    .mapResult({ result ->
                        result.data.map { repositoryItemModelMapper.mapFrom(it) }.asResult()

                    }, { it })
}