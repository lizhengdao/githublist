package ru.adigyran.githublist.data.models.classes

import ru.ivan.core.server.base.IServerError

/**
 * Created by i.sokolovskiy on 25.02.20.
 */
data class ServerError(
    val _code: String,
    var _detail: String
) : IServerError {
    override fun getCode(): String = _code

    override fun getDetail(): String = _detail
}