package ru.adigyran.githublist.data.mappers

import ru.adigyran.githublist.data.mappers.interfaces.IRepositoryItemModelMapper
import ru.adigyran.githublist.data.models.classes.RepositoryItemModel
import ru.adigyran.githublist.server.response.RepoResp

class RepositoryItemModelMapperImpl : IRepositoryItemModelMapper() {
    override fun mapFrom(type: RepoResp): RepositoryItemModel =
            RepositoryItemModel(
                    id = type.id?:-1,
                    name = type.name?:"",
                    url = type.htmlUrl?:"",
                    fullName = type.fullName?:"",
                    forksCount = type.forksCount?:0,
                    stargazersCount = type.stargazersCount?:0,
                    watchersCount = type.watchersCount?:0
                    )

}