package ru.adigyran.githublist.data.models.classes

class RepositoryItemModel(val id: Int,
                          val name: String,
                          val url:String,
                          val fullName: String,
                          val forksCount: Int,
                          val stargazersCount: Int,
                          val watchersCount: Int) {
}