package ru.adigyran.githublist.app.reposlist.mvi


sealed class UiEvent {
    class UsernameChanging(val text:String) : UiEvent()
    class RepositoryClicked(val id:Int) : UiEvent()
    object ExecuteSearch : UiEvent()
    object RefreshData : UiEvent()
    object Back : UiEvent()
}