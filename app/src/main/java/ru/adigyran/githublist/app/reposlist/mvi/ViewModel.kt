package ru.adigyran.githublist.app.reposlist.mvi

import ru.ivan.core.ui.adapters.base.DiffItem


data class ViewModel(
    val data: List<DiffItem>,
    val isPullToRefreshVisible: Boolean,
    val isMainLoaderVisible: Boolean,
    val isEmptyDatasetVisible: Boolean,
    val emptySearchFlag:Boolean,
    val userName:String
    )