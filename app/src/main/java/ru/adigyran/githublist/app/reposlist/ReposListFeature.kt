package ru.adigyran.githublist.app.reposlist

import android.content.Context
import com.badoo.mvicore.element.*
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import ru.adigyran.githublist.app.root.RootView
import ru.adigyran.githublist.data.interactors.RepositoriesInteractor
import ru.adigyran.githublist.data.models.classes.RepositoryItemModel
import ru.ivan.core.ext.observeOnMainThread
import ru.ivan.core.ui.BaseFeature
import ru.ivan.core.unclassifiedcommonmodels.dataprocessing.*
import ru.ivan.core.unclassifiedcommonmodels.navigation.coordinator.CoordinatorEvent
import ru.ivan.core.unclassifiedcommonmodels.navigation.coordinator.CoordinatorRouter


class ReposListFeature(
        coordinatorRouter: CoordinatorRouter,
        context: Context,
        repositoriesInteractor: RepositoriesInteractor,
        param: ReposListView.Param
) :
        BaseFeature<ReposListFeature.Wish, ReposListFeature.Action, ReposListFeature.Effect, ReposListFeature.State, ReposListFeature.News>(
                initialState = State(
                ),
                bootstrapper = BootstrapperImpl(),
                wishToAction = {
                    when (it) {
                        is Wish.RefreshData -> Action.LoadData(LoadingType.REFRESH)
                        is Wish.ExecuteSearch -> Action.LoadData(LoadingType.LOAD)
                        else -> Action.Execute(it)
                    }
                },
                actor = ActorImpl(
                        coordinatorRouter,
                        repositoriesInteractor
                ),
                reducer = ReducerImpl(),
                postProcessor = PostProcessorImpl(),
                newsPublisher = NewsPublisherImpl(context)
        ) {

    data class State(
            val dataOperation: DataLoadingOperation = DataLoadingOperation.Idle,
            val data: List<RepositoryItemModel>? = null,
            val emptySearchFlag: Boolean = false,
            val username:String = ""
    )

    sealed class Wish {
        class UsernameChanging(val text: String) : Wish()
        class OpenRepository(val id:Int) : Wish()
        object RefreshData : Wish()
        object ExecuteSearch : Wish()
        object Finish : Wish()
    }

    sealed class Action {
        data class LoadData(val loadingType: LoadingType) : Action()
        data class Execute(val wish: Wish) : Action()
        object ClearData : Action()
        object CancelLoading : Action()
    }

    sealed class Effect {
        data class DataProcessingEffect(val loadingEffect: DataLoadingEffect<RepositoryItemModel>) :
                Effect()

        data class UserNameChanged(val text: String) : Effect()
        object DataCleared : Effect()
        object EventSend : Effect()

        object LoadingCancelled : Effect()
    }

    sealed class News {
        data class Base(val news: BaseNews) : News()
        object DataSetChanged : News()

    }

    class ActorImpl(
            private val coordinatorRouter: CoordinatorRouter,
            private val repositoriesInteractor: RepositoriesInteractor
    ) :
            Actor<State, Action, Effect> {
        private val interruptLoadDataSignal = PublishSubject.create<Unit>()

        override fun invoke(state: State, action: Action): Observable<Effect> {
            return when (action) {
                is Action.Execute -> when (action.wish) {

                    is Wish.OpenRepository ->
                    {
                        if(action.wish.id==-1) return Observable.empty()
                        val repositoryItemModel = state.data?.find { it.id == action.wish.id }
                        repositoryItemModel?.let {model->
                            val url  = model.url
                            if(url.isEmpty()) return Observable.empty()
                            return Observable
                                    .fromCallable {
                                        coordinatorRouter.sendEvent(
                                                RootView.Events.OpenUrl(url)
                                        )
                                        Effect.EventSend as Effect
                                    }.observeOnMainThread()

                        }?:Observable.empty()
                    }
                    is Wish.UsernameChanging -> {
                        if (action.wish.text == state.username) return Observable.empty()
                        return Observable.just(Effect.UserNameChanged(action.wish.text) as Effect)
                    }
                    Wish.RefreshData -> return Observable.empty<Effect>()
                            .observeOnMainThread()//см. wishToAction
                    Wish.ExecuteSearch -> return Observable.empty<Effect>()
                            .observeOnMainThread()//см. wishToAction
                    is Wish.Finish -> {
                        Observable.just(Effect.EventSend as Effect)
                                .doOnNext { coordinatorRouter.sendEvent(RootView.Events.Exit) }
                                .observeOnMainThread()
                    }
                    else -> Observable.empty()
                }
                is Action.ClearData -> return Observable.just(Effect.DataCleared as Effect)
                        .observeOnMainThread()
                is Action.LoadData -> {
                    val loadingType = action.loadingType
                    val searchText = state.username.trim()
                    if(searchText.isEmpty()) return Observable.empty()
                    interruptLoadDataSignal.onNext(Unit)
                    repositoriesInteractor.getUserRepositories(searchText)
                            .takeUntil(interruptLoadDataSignal)
                            .map {
                                it.processResult(
                                        {
                                            when (loadingType) {
                                                LoadingType.LOAD -> Effect.DataProcessingEffect(
                                                        DataLoadingEffect.Loaded(
                                                                it.data
                                                        )
                                                ) as Effect
                                                LoadingType.REFRESH -> Effect.DataProcessingEffect(
                                                        DataLoadingEffect.Refreshed(it.data)
                                                ) as Effect
                                            }
                                        },
                                        {
                                            when (loadingType) {
                                                LoadingType.LOAD -> Effect.DataProcessingEffect(
                                                        DataLoadingEffect.LoadingError(it)
                                                ) as Effect
                                                LoadingType.REFRESH -> Effect.DataProcessingEffect(
                                                        DataLoadingEffect.RefreshingError(it)
                                                ) as Effect
                                            }
                                        }
                                )
                            }
                            .observeOnMainThread()
                            .startWith(
                                    when (loadingType) {
                                        LoadingType.LOAD -> Effect.DataProcessingEffect(
                                                DataLoadingEffect.Loading()
                                        ) as Effect
                                        LoadingType.REFRESH -> Effect.DataProcessingEffect(
                                                DataLoadingEffect.Refreshing()
                                        ) as Effect
                                    }
                            )
                }
                is Action.CancelLoading -> {
                    interruptLoadDataSignal.onNext(Unit)

                    Observable.just(Effect.LoadingCancelled as Effect)
                            .observeOnMainThread()
                }
                else -> Observable.empty()
            }
        }
    }

    class ReducerImpl : Reducer<State, Effect> {
        override fun invoke(state: State, effect: Effect): State {
            return when (effect) {

                is Effect.DataProcessingEffect -> when (effect.loadingEffect) {
                    is DataLoadingEffect.Loading -> state.copy(
                            dataOperation = DataLoadingOperation.LoadingData(OperationState.PROGRESS)
                    )
                    is DataLoadingEffect.Loaded -> state.copy(
                            dataOperation = DataLoadingOperation.Idle,
                            data = effect.loadingEffect.payload,
                            emptySearchFlag = effect.loadingEffect.payload.isEmpty()
                    )
                    is DataLoadingEffect.LoadingError -> state.copy(
                            dataOperation = DataLoadingOperation.LoadingData(OperationState.NETWORK_ERROR)
                    )
                    is DataLoadingEffect.Refreshing -> state.copy(
                            dataOperation = DataLoadingOperation.RefreshingData(OperationState.PROGRESS)
                    )
                    is DataLoadingEffect.Refreshed -> state.copy(
                            dataOperation = DataLoadingOperation.Idle,
                            data = effect.loadingEffect.payload,
                            emptySearchFlag = effect.loadingEffect.payload.isEmpty()
                    )
                    is DataLoadingEffect.RefreshingError -> state.copy(
                            dataOperation = DataLoadingOperation.LoadingData(OperationState.NETWORK_ERROR)
                    )
                }
                is Effect.UserNameChanged -> state.copy(username = effect.text)
                is Effect.DataCleared -> state.copy(
                        data = null
                )
                is Effect.LoadingCancelled -> state.copy(dataOperation = DataLoadingOperation.Idle)
                Effect.EventSend -> state.copy()
                else -> state.copy()
            }
        }
    }

    class BootstrapperImpl : Bootstrapper<Action> {
        override fun invoke(): Observable<Action> = Observable.merge<Action>(
                listOf(
                      //Observable.just(Action.LoadData(LoadingType.LOAD)),

                )
        ).observeOnMainThread()
    }

    class PostProcessorImpl : PostProcessor<Action, Effect, State> {
        // do anything based on action (contains wish), effect, state
        override fun invoke(action: Action, effect: Effect, state: State): Action? {
            if (effect is Effect.DataProcessingEffect && effect.loadingEffect is DataLoadingEffect.Loading) {
                return Action.ClearData
            }
            if (effect is Effect.UserNameChanged) {
                return Action.CancelLoading
            }
            return null
        }
    }

    class NewsPublisherImpl(val context: Context) : NewsPublisher<Action, Effect, State, News> {
        override fun invoke(action: Action, effect: Effect, state: State): News? = when (effect) {
            is Effect.DataProcessingEffect -> when (effect.loadingEffect) {
                is DataLoadingEffect.LoadingError -> processError(
                        context, effect.loadingEffect.throwable
                )?.let { News.Base(it) as News }
                is DataLoadingEffect.Loaded -> News.DataSetChanged

                is DataLoadingEffect.RefreshingError -> processError(
                        context, effect.loadingEffect.throwable
                )?.let { News.Base(it) as News }
                is DataLoadingEffect.Refreshed -> News.DataSetChanged
                else -> null
            }
            else -> null
        }
    }

    sealed class Events : CoordinatorEvent() {
        data class OpenRequestCard(val requestId: Int) : Events()
        object Finish : Events()
    }
}