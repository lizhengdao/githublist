package ru.adigyran.githublist.app.root.mvi

import androidx.lifecycle.Lifecycle
import com.badoo.mvicore.android.lifecycle.StartStopBinderLifecycle
import com.badoo.mvicore.binder.Binder
import com.badoo.mvicore.binder.using
import io.reactivex.ObservableSource
import io.reactivex.functions.Consumer
import ru.adigyran.githublist.app.root.RootFeature
import ru.adigyran.githublist.app.root.RootFeature.*

import ru.ivan.core.ui.BindingsBase

class Bindings constructor(lifecycleOwner: Lifecycle, private val feature: RootFeature):
    BindingsBase<ViewModel, UiEvent, News>()  {
    private val binder: Binder = Binder(StartStopBinderLifecycle(lifecycleOwner))

    override fun setup(
        viewModelConsumer: Consumer<ViewModel>, uiEventsObservableSource: ObservableSource<UiEvent>,
        newsConsumer: Consumer<News>
    ) {
        binder.bind(uiEventsObservableSource to feature using UiEventTransformer())
        binder.bind(feature to viewModelConsumer using ViewModelTransformer())
        binder.bind(feature.news to newsConsumer)
    }

    private class UiEventTransformer : (UiEvent) -> Wish? {
        override fun invoke(event: UiEvent): Wish? = when (event) {

            else -> null
        }
    }

    private class ViewModelTransformer : (State) -> ViewModel {
        override fun invoke(state: State): ViewModel =
            ViewModel(
                stubParam = state.stubParam
            )
    }
}