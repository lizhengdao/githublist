package ru.adigyran.githublist.app.reposlist.mvi

import android.content.Context
import androidx.lifecycle.Lifecycle
import com.badoo.mvicore.android.lifecycle.StartStopBinderLifecycle
import com.badoo.mvicore.binder.Binder
import com.badoo.mvicore.binder.using
import io.reactivex.ObservableSource
import io.reactivex.functions.Consumer
import ru.adigyran.githublist.app.reposlist.ReposListFeature
import ru.adigyran.githublist.app.reposlist.ReposListFeature.*
import ru.adigyran.githublist.ui.delegates.UserRepoItemUIModel

import ru.ivan.core.ui.BindingsBase
import ru.ivan.core.ui.adapters.base.DiffItem
import ru.ivan.core.unclassifiedcommonmodels.dataprocessing.DataLoadingOperation
import ru.ivan.core.unclassifiedcommonmodels.dataprocessing.OperationState


class Bindings constructor(
        lifecycleOwner: Lifecycle,
        private val feature: ReposListFeature,
        private val context: Context
) : BindingsBase<ViewModel, UiEvent, News>() {
    private val binder: Binder = Binder(StartStopBinderLifecycle(lifecycleOwner))

    override fun setup(
            viewModelConsumer: Consumer<ViewModel>, uiEventsObservableSource: ObservableSource<UiEvent>,
            newsConsumer: Consumer<News>
    ) {
        binder.bind(uiEventsObservableSource to feature using UiEventTransformer())
        binder.bind(feature to viewModelConsumer using ViewModelTransformer(context))
        binder.bind(feature.news to newsConsumer)
    }

    private class UiEventTransformer : (UiEvent) -> Wish? {
        override fun invoke(event: UiEvent): Wish? = when (event) {
            is UiEvent.RefreshData -> Wish.RefreshData
            is UiEvent.UsernameChanging -> Wish.UsernameChanging(event.text)
            is UiEvent.RepositoryClicked -> Wish.OpenRepository(event.id)
            is UiEvent.ExecuteSearch -> Wish.ExecuteSearch
            is UiEvent.Back -> Wish.Finish
            else -> null
        }
    }

    private class ViewModelTransformer(private val context: Context) :
            (State) -> ViewModel {
        override fun invoke(state: State): ViewModel =
                ViewModel(

                        isMainLoaderVisible = (state.dataOperation as? DataLoadingOperation.LoadingData)?.operationState == OperationState.PROGRESS && state.data.isNullOrEmpty(),
                        isPullToRefreshVisible = (state.dataOperation as? DataLoadingOperation.RefreshingData)?.operationState == OperationState.PROGRESS
                                || ((state.dataOperation as? DataLoadingOperation.LoadingData)?.operationState == OperationState.PROGRESS && state.data?.isNotEmpty() == true),

                        /* kotlin.run {
                            mutableListOf<DiffItem>().apply {
                                state.data?.map {
                                    ItemCategoryUIModel(
                                        it.id,
                                        it.name,
                                        it.imageUrl,
                                        it.orientation
                                    )
                                }
                                    ?.let { addAll(it) }
                                if ((state.dataOperation as? DataLoadingOperationWithPagination.NextPageLoading)?.operationState == OperationState.PROGRESS) add(
                                    LoadMoreFooterUIModel2()
                                )
                            }
                        },*/

                        data = kotlin.run {
                            mutableListOf<DiffItem>().apply {
                                state.data?.map {
                                    UserRepoItemUIModel(
                                            id = it.id,
                                            repoName = it.name,
                                            fullName = it.fullName,
                                            forksCount = it.forksCount,
                                            watchersCount = it.watchersCount,
                                            stargazersCount = it.stargazersCount
                                            )
                                }
                                        ?.let { addAll(it) }
                            }

                        },
                        isEmptyDatasetVisible = state.dataOperation is DataLoadingOperation.Idle && state.data?.isEmpty() == true,
                        emptySearchFlag = state.emptySearchFlag,
                        userName = state.username

                )
    }
}