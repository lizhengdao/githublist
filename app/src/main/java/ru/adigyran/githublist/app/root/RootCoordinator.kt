package ru.adigyran.githublist.app.root

import android.content.Context
import android.util.Log
import androidx.fragment.app.FragmentManager
import ru.ivan.core.BaseScreens

import ru.ivan.core.unclassifiedcommonmodels.navigation.coordinator.Coordinator
import ru.ivan.core.unclassifiedcommonmodels.navigation.coordinator.CoordinatorEvent
import ru.terrakok.cicerone.Router
import java.lang.ref.WeakReference

class RootCoordinator : Coordinator {
    private val navigationRouter: Router
    private val contextWeak: WeakReference<Context>
    private val fragmentManagerWeak: WeakReference<FragmentManager>

    constructor(
        navigationRouter: Router,
        context: Context,
        fragmentManager: FragmentManager
    ) {
        this.navigationRouter = navigationRouter
        this.contextWeak = WeakReference(context)
        this.fragmentManagerWeak = WeakReference(fragmentManager)
    }

    override fun consumeEvent(event: CoordinatorEvent) {
        Log.d(RootCoordinator::class.java.name, "consumeEvent: $event")
        when (event) {

            is RootView.Events.OpenUrl -> navigationRouter.navigateTo(
                    BaseScreens.WebBrowser(event.url)
            )
            RootView.Events.Exit -> navigationRouter.exit()
            RootView.Events.ExitApp -> {

                navigationRouter.finishChain()
            }
        }
    }
}