package ru.adigyran.githublist.app.root

import androidx.annotation.IdRes
import androidx.fragment.app.FragmentActivity
import org.jetbrains.anko.browse
import ru.ivan.core.BaseScreens
import ru.ivan.core.ext.closeKeyboard
import ru.ivan.core.ext.email
import ru.terrakok.cicerone.android.support.SupportAppNavigator
import ru.terrakok.cicerone.commands.Command
import ru.terrakok.cicerone.commands.Forward

class RootNavigator (private val activity: FragmentActivity, @IdRes private val containerId: Int) :
    SupportAppNavigator(activity, containerId) {
    override fun applyCommands(commands: Array<out Command>) {
        activity.runOnUiThread {
            activity.closeKeyboard()
            super.applyCommands(commands)
        }
    }

    override fun activityBack() {
        activity.moveTaskToBack(true)
    }

    override fun applyCommand(command: Command) {
        when (command) {
            is Forward -> {
                val screen = command.screen
                when (screen) {
                    is BaseScreens.WebBrowser -> {
                        activity.browse((command.screen as BaseScreens.WebBrowser).site)
                        return
                    }
                    is BaseScreens.Email -> {
                        activity.email((command.screen as BaseScreens.Email).mail)
                        return
                    }


                }
            }
        }



        super.applyCommand(command)
    }
}