package ru.adigyran.githublist.app.reposlist

import android.annotation.SuppressLint
import android.os.Bundle
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.WindowManager
import androidx.lifecycle.Lifecycle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.badoo.mvicore.ModelWatcher
import com.badoo.mvicore.modelWatcher
import com.jakewharton.rxbinding3.appcompat.queryTextChangeEvents
import io.reactivex.disposables.Disposable
import io.reactivex.disposables.Disposables
import io.reactivex.functions.Consumer
import kotlinx.android.parcel.Parcelize
import org.kodein.di.generic.factory
import org.kodein.di.generic.on
import ru.adigyran.githublist.R
import ru.adigyran.githublist.app.reposlist.di.reposListDiModule
import ru.adigyran.githublist.app.reposlist.mvi.Bindings
import ru.adigyran.githublist.app.reposlist.mvi.UiEvent
import ru.adigyran.githublist.app.reposlist.mvi.ViewModel
import ru.adigyran.githublist.databinding.ViewReposListBinding
import ru.adigyran.githublist.ui.delegates.UserRepoItemDelegate
import ru.ivan.core.ext.*
import ru.ivan.core.ui.DiFragment
import ru.ivan.core.ui.adapters.base.BaseDelegateAdapter
import ru.ivan.core.ui.views.VerticalListTopBottomSpaceDecoration
import ru.ivan.core.unclassifiedcommonmodels.navigation.BackButtonListener
import java.util.concurrent.TimeUnit


class ReposListView :
        DiFragment<ViewReposListBinding, ViewModel, UiEvent, ReposListFeature.News>(),
        BackButtonListener {
    private var param: Param by argument("param")
    override val statusBarId: Int? = R.color.black
    override val inputMode: Int = WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN


    private val bindingsFactory: ((lifecycleOwner: Lifecycle) -> Bindings) by kodein.on(context = this)
            .factory()
    //endregion

    override fun provideViewBinding(
            inflater: LayoutInflater, container: ViewGroup?
    ): ViewReposListBinding =
            ViewReposListBinding.inflate(inflater, container, false)
    //region MVI

    override fun processNews(news: ReposListFeature.News) =
            when (news) {
                is ReposListFeature.News.Base -> processBaseNews(news.news)
                ReposListFeature.News.DataSetChanged -> {
                }
            }

    override val viewModelConsumer = Consumer<ViewModel> {
        binding.swipe.isRefreshing =
                it.isPullToRefreshVisible
        viewModelWatcher(it)
    }

    //endregion


    private val adapter = BaseDelegateAdapter.create {
        delegate {
            UserRepoItemDelegate({
                sendUiEvent(UiEvent.RepositoryClicked(it))
            })
        }

    }





    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)

    }

    private var queryTextChangesDisposable: Disposable = Disposables.disposed()

    override fun onDestroyView() {
        queryTextChangesDisposable.dispose()
        binding.searchView.setOnQueryTextFocusChangeListener(null)
        super.onDestroyView()
    }

    override fun onResume() {

        showSoftKeyboard(view?.findFocus() ?: binding.searchView)
        super.onResume()
    }


    @SuppressLint("UseCompatLoadingForDrawables")
    override fun provideModelWatcher(): ModelWatcher<ViewModel> = modelWatcher {

        ViewModel::userName {
            if (binding.searchView.query?.toString() != it) {
                binding.searchView.setQuery(it, false)
            }
        }
        ViewModel::data {

            adapter.items = it
            if (binding.recycler.adapter == null) {
                binding.recycler.swapAdapter(adapter, true)
            }
        }

        ViewModel::isMainLoaderVisible { binding.rlProgress.toggleVisibility(it) }

        ViewModel::emptySearchFlag{

        }
    }

    override fun prepareUi(savedInstanceState: Bundle?) {
        super.prepareUi(savedInstanceState)

        viewModelWatcher


        binding.swipe.setOnRefreshListener { sendUiEvent(UiEvent.RefreshData) }


        with(binding.recycler) {
            itemAnimator = null
            val lm = LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
            setItemViewCacheSize(6)
            layoutManager = lm
            addItemDecoration(VerticalListTopBottomSpaceDecoration(10.dpToPx(), false, false))
            clearOnScrollListeners()

        }


        queryTextChangesDisposable.dispose()
        queryTextChangesDisposable = binding.searchView.queryTextChangeEvents()
                .distinctUntilChanged { t1, t2 -> t1.isSubmitted == t2.isSubmitted && t1.queryText.toString() == t2.queryText.toString() }
                .skip(1)
                .doOnNext {
                    sendUiEvent(UiEvent.UsernameChanging(it.queryText.toString()))

                    if (it.isSubmitted) binding.searchView.clearFocus()
                }
                .debounce(500, TimeUnit.MILLISECONDS)
                .observeOnMainThread()
                .doOnNext {
                    if (!it.isSubmitted) sendUiEvent(UiEvent.ExecuteSearch)
                }
                .subscribe()

        binding.searchView.setOnQueryTextFocusChangeListener { _, focused ->
            if (focused) showSoftKeyboard(view!!.findFocus())
        }

    }


    override fun provideBindings() = bindingsFactory(lifecycle)
    override fun provideDiModule() =
            reposListDiModule(tryTyGetParentRouter(), param)

    @Parcelize
    data class Param(
            val stub: Boolean = false
    ) : Parcelable

    override fun onBackPressed(): Boolean {
        sendUiEvent(UiEvent.Back)
        return true
    }
}