package ru.adigyran.githublist.util

import com.squareup.moshi.JsonDataException
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import ru.adigyran.githublist.data.models.classes.ServerError
import ru.adigyran.githublist.server.response.ErrorType1Resp
import ru.adigyran.githublist.server.response.ErrorType2Resp
import ru.ivan.core.moshi.adapters.BigDecimalJsonAdapter
import ru.ivan.core.moshi.adapters.FallbackIntegerEnumJsonAdapter
import ru.ivan.core.server.base.IServerError
import java.io.IOException
import java.lang.reflect.Type

object Utils {
    private val moshi: Moshi =
            Moshi.Builder()
                    .add(FallbackIntegerEnumJsonAdapter.ADAPTER_FACTORY)
                    .add(BigDecimalJsonAdapter())
                    .add(KotlinJsonAdapterFactory()).build()
    @Throws(JsonDataException::class, IOException::class)
    fun <T> fromJson(json: String, classOfT: Class<T>): T? = moshi.adapter(classOfT).fromJson(json)

    @Throws(JsonDataException::class, IOException::class)
    fun <T> fromJson(json: String, typeOfT: Type): T? = moshi.adapter<T>(typeOfT).fromJson(json)

    fun toJson(obj: Any): String = moshi.adapter<Any>(Object::class.java).toJson(obj)
    fun mapServerErrors(errorString: String): List<IServerError> {
        var errorResp1: ErrorType1Resp? = null
        var errorResp2: ErrorType2Resp? = null
        errorResp1 = try {
            fromJson<ErrorType1Resp>(errorString, ErrorType1Resp::class.java)
        } catch (ex: JsonDataException) {
            null
        }

        if (errorResp1 == null) {
            errorResp2 = try {
                fromJson<ErrorType2Resp>(errorString, ErrorType2Resp::class.java)
            } catch (ex: JsonDataException) {
                null
            }
        }

        return when {
            errorResp1 != null -> mutableListOf<IServerError>().apply {
                errorResp1.errors.entries.map { entry ->
                    entry.value.forEach { add(ServerError(errorResp1.errorCode ?: "", it)) }
                }
            }
            errorResp2 != null -> mutableListOf<IServerError>().apply {
                errorResp2.errors.entries.map { entry ->
                    add(ServerError(errorResp2.errorCode ?: "", entry.value))
                }
            }
            else -> listOf()
        }
    }
}