package ru.adigyran.githublist.util.moshi

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import org.joda.time.DateTime
import org.joda.time.format.ISODateTimeFormat


class JodaDateTimeAdapter {
    @FromJson
    fun fromJson(date: String): DateTime = ISODateTimeFormat.basicDateTime().parseDateTime(date)

    @ToJson
    fun toJson(date: DateTime): String = date.toString(ISODateTimeFormat.basicDateTime())
}