package ru.adigyran.githublist.di

import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton
import retrofit2.Retrofit
import ru.adigyran.githublist.server.Api


fun apiDiModule() = Kodein.Module("apiDiModule") {
    bind<Api>() with singleton { instance<Retrofit>("Api").create(Api::class.java) }
}