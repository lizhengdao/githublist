package ru.adigyran.githublist.di

import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import ru.adigyran.githublist.data.interactors.RepositoriesInteractor


fun interactorsDiModule() = Kodein.Module("interactorsDiModule") {
    bind() from provider { RepositoriesInteractor(instance(),instance()) }

}