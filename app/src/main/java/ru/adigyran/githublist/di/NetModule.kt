package ru.adigyran.githublist.di

import android.content.Context
import com.facebook.stetho.okhttp3.StethoInterceptor
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import ru.adigyran.githublist.BuildConfig
import ru.adigyran.githublist.util.moshi.JodaDateTimeAdapter
import ru.ivan.core.moshi.adapters.BigDecimalJsonAdapter
import ru.ivan.core.moshi.adapters.FallbackIntegerEnumJsonAdapter
import ru.ivan.core.moshi.adapters.NullPrimitiveAdapter
import java.util.concurrent.TimeUnit


fun netDiModule(context: Context) = Kodein.Module("netDiModule") {





    bind<OkHttpClient>("Api") with singleton {
        val logging = HttpLoggingInterceptor()
        logging.level =
            if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE


        val builder = OkHttpClient.Builder()
            .addInterceptor(logging)
        // .addInterceptor(ChuckerInterceptor(context))

        if (BuildConfig.DEBUG) builder.addNetworkInterceptor(StethoInterceptor())

        return@singleton builder
            .connectTimeout(30, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)
            .build()
    }
    bind<Retrofit>("Api") with singleton {
        return@singleton Retrofit.Builder()
            .baseUrl(BuildConfig.API_ENDPOINT)
            .client(instance("Api"))
            .addConverterFactory(
                MoshiConverterFactory.create(
                    Moshi.Builder()
                        .add(NullPrimitiveAdapter())
                        .add(FallbackIntegerEnumJsonAdapter.ADAPTER_FACTORY)
                        .add(BigDecimalJsonAdapter())
                        .add(JodaDateTimeAdapter())
                        .add(KotlinJsonAdapterFactory()).build()
                )
            )
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
    }


}