package ru.adigyran.githublist.di

import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import ru.adigyran.githublist.data.mappers.RepositoryItemModelMapperImpl
import ru.adigyran.githublist.data.mappers.interfaces.IRepositoryItemModelMapper


fun mappersDiModule() = Kodein.Module("mappersDiModule") {

    bind<IRepositoryItemModelMapper>() with provider { RepositoryItemModelMapperImpl() }
}